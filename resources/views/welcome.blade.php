<!DOCTYPE html>
<html>
    <head>
        <title>Facebook Message History Extractor</title>
		<meta name="csrf-token" content="{{ csrf_token() }}" />
		<meta property="og:url" content="{{ url('/') }}" />
		<meta property="og:type" content="website" />
		<meta property="og:title" content="Facebook Message History Extractor" />
		<meta property="og:description" content="Easily extract your conversation history with someone from your Facebook data." />
		<meta property="og:image" content="{{ url('/') }}/resources/img/avatar-ninja.png" />
		<meta property="fb:app_id" content="1148782061860671" />
		<link rel="apple-touch-icon" sizes="57x57" href="/apple-icon-57x57.png">
		<link rel="apple-touch-icon" sizes="60x60" href="/apple-icon-60x60.png">
		<link rel="apple-touch-icon" sizes="72x72" href="/apple-icon-72x72.png">
		<link rel="apple-touch-icon" sizes="76x76" href="/apple-icon-76x76.png">
		<link rel="apple-touch-icon" sizes="114x114" href="/apple-icon-114x114.png">
		<link rel="apple-touch-icon" sizes="120x120" href="/apple-icon-120x120.png">
		<link rel="apple-touch-icon" sizes="144x144" href="/apple-icon-144x144.png">
		<link rel="apple-touch-icon" sizes="152x152" href="/apple-icon-152x152.png">
		<link rel="apple-touch-icon" sizes="180x180" href="/apple-icon-180x180.png">
		<link rel="icon" type="image/png" sizes="192x192"  href="/android-icon-192x192.png">
		<link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
		<link rel="icon" type="image/png" sizes="96x96" href="/favicon-96x96.png">
		<link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
		<link rel="manifest" href="/manifest.json">
		<meta name="msapplication-TileColor" content="#ffffff">
		<meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
		<meta name="theme-color" content="#ffffff">
        <link href="/resources/css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <link href="/resources/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css">
        <link href="/resources/css/fileinput.min.css" rel="stylesheet" type="text/css">
        <link href="/resources/css/bootstrap-select.min.css" rel="stylesheet" type="text/css">
        <link href="/resources/css/main.css" rel="stylesheet" type="text/css">
        <link href="/resources/css/font-awesome.min.css" rel="stylesheet" stype="text/css">
        <script src='https://www.google.com/recaptcha/api.js'></script>
    </head>
    <body>
        <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
        ga('create', 'UA-80310156-1', 'auto');
        ga('send', 'pageview');
        </script>
		<div id="fb-root"></div>
		<script>(function(d, s, id) {
		  var js, fjs = d.getElementsByTagName(s)[0];
		  if (d.getElementById(id)) return;
		  js = d.createElement(s); js.id = id;
		  js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.7&appId=1148782061860671";
		  fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));</script>
    	<nav class="header">
			<div>
				<ul class="topnav">
					<li><a href="/" class="header-title"><span class="glyphicon glyphicon-home"></span>&nbsp;&nbsp; Facebook Conversation History Extractor</a></li>
					<li style="float: right"><img src="/resources/img/avatar-ninja-48x45.png" alt="Ninja" class="header-ninja"/></li>
					<li style="float: right"><a class="link js-feedback" href="#">Feedback</a></li>
					<li style="float: right"><a class="link js-privacy" href="#">Privacy and Terms</a></li>
					<li style="float: right"><a class="link js-how-to-use" href="#">How to Use</a></li>
				</ul>
			</div>
		</nav>

        <div class="container">
			<div class="welcome-message">
				<h1>Welcome</h1>
				<p>This tool allows you to extract specific conversations with people that you have had from Facebook (either through the Facebook website or messenger) and give it to you in a simple text file.</p>
				<p>Why does this tool exist? I needed to provide my conversation history with my partner for our Marriage visa application and even though Facebook allows you to download your entire Facebook history, it’s not really in a suitable format for what we needed. So I wrote this tool to make this process simpler and hopefully you can also find a use for it.</p>
				<p>For this tool to work you will need to first get your Facebook history and upload a specific file from it called messages.htm (if you do not know how to do this <a class="link js-how-to-use" href="#">click here</a> and follow the steps). The Maximum size of this file allowed by this tool is 20 Megabytes.</p>
				<p>This tool does not actually log into your Facebook account and only cleans up the data that Facebook already provides to you. It does not process any images, video or emoji’s you send to people as this information is not provided in the existing Facebook message archive.</p>
				<div class="alert alert-danger" role="alert">
					<span><i class="fa fa-trash fa-lg" aria-hidden="true"></i> Your data is automatically purged from the server after 20 minutes. Please read our <a href="#" class="js-privacy">privacy statement</a> for more information.</span>
				</div>
			</div>

			<div class="panel panel-default">
				<div class="panel-heading">
					Step 1: Upload your facebook message.htm file (If you don't know how to get this <a class="js-how-to-use" href="#">Click Here</a>). Maximum file size is 20 Megabytes.
				</div>
				<div class="panel-body">
	                {{ Form::open(array('id' => 'historyFileUploadForm', 'files' => true)) }}
   					<div class="row">
	   					<div class="col-xs-12">
						    <div class="form-group">
						        {{ Form::label('historyFile', 'Your History Archive', ['class' => 'control-label']) }}
					            {{ Form::file('historyFile', ['class' => 'file-loading']) }}
					        </div>
					    </div>
				    </div>

   					<div class="row">
   						<div class="col-xs-12">
			            	{{ Form::button('Upload', ['id' => 'historyFileSubmit',  'class' => 'btn btn-inverse center-block']) }}
			            </div>
				    </div>
	                {{ Form::close() }}
	            </div>
            </div>

			<div class="panel panel-default disabled">
				<div class="panel-heading">
					Step 2: Select the conversation you wish to extract and download (you can do this multiple times)
				</div>
				<div class="panel-body">
					{{ Form::open(array('id' => 'conversationSelectForm')) }}
					{{ Form::hidden('fileToProcess', null, ['id' => 'fileToProcess']) }}
					<div class="row">
					    <div class="col-xs-6">
					        <div class="form-group">
					        	{{ Form::label('iAmCombo', 'I Am', ['class' => 'control-label']) }}
					        	{{ Form::select('iAmCombo', [], null, ['class' => 'form-control selectpicker show-tick', 'required']) }}
					        </div>
					    </div>
					    <div class="col-xs-6">
					        <div class="form-group">
					        	{{ Form::label('whoWithCombo', 'Who I would get my conversation history with', ['class' => 'control-label']) }}
					        	{{ Form::select('whoWithCombo', [], null, ['class' => 'form-control selectpicker show-tick', 'data-live-search' => 'true', 'required', 'liveSearchNormalize' => 'true']) }}
					        </div>
					    </div>
					</div>
					<div class="row">
					    <div class="col-xs-12">
							{{ Form::button('<span class="glyphicon glyphicon-download"></span> Extract and Download', ['id' => 'conversationExtractSubmit', 'disabled', 'class' => 'btn btn-inverse center-block']) }}
						</div>
					</div>
					{{ Form::close() }}
            	</div>
            </div>

			<div class="row">
   				<div class="col-xs-12">
					<div class="alert alert-info" role="alert">
			   			<i class="fa fa-info-circle" aria-hidden="true"></i> <small>This tool has only been tested with an english archive. If you have problems with a different language you can help by sending me the first line of the messages.htm (by opening it up in a text editor) and using the feedback form to send it to us.</small>
		   			</div>
   				</div>
   			</div>

			<div class="row">
   				<div class="col-xs-12">
	   				<div class="fb-like pull-right" data-href="https://fbextract.leathley.ninja/" data-layout="button" data-action="recommend" data-size="small" data-show-faces="true" data-share="true"></div>
   				</div>
   			</div>
        </div>

		<div class="modal modal-static fade" id="processing-modal" tabindex="-1" role="dialog" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-body">
						<div class="text-center" id="processingGif">
							<img src="/resources/img/processing.gif" class="icon" />
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="modal fade" id="how-to-use-modal" tabindex="-1" role="dialog">
			<div class="modal-dialog modal-lg2">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title">How to Use</h4>
					</div>
					<div class="modal-body how-to-use-modal-body">
						<div class="row">
   							<div class="col-xs-7">
   								<h4>Step 1</h4>
	   							<p>You will first need to log into your Facebook account and click on the top right down arrow (<i class="fa fa-caret-down" aria-hidden="true"></i>) which will open the navigation menu.</p><p>You will then have to click the Settings menu item.</p>
   							</div>
   							<div class="col-xs-5">
	   							<img src="/resources/img/fb-settings.png" alt="Facebook Settings" class="img-responsive img-thumbnail">
   							</div>
						</div>

						<div class="row top-buffer">
   							<div class="col-xs-5">
	   							<img src="/resources/img/fb-download-button.png" alt="Facebook Settings" class="img-responsive img-thumbnail">
   							</div>
   							<div class="col-xs-7">
   								<h4>Step 2</h4>
	   							<p>The next page will show you your General Account Settings (in the middle of the screen).<br/>
	   							<br/>
	   							At the bottom of this is a link to Download a copy of your Facebook data. You will need to click this.</p>
   							</div>
						</div>

						<div class="row top-buffer">
   							<div class="col-xs-12">
								<h4>Step 3</h4>
								<p>After clicking the Download button, you will be given the option to Start the Archive. You will need to click the green <span class="btn btn-xs btn-success">Start My Archive</span> button in the middle.<br/>
								<br/>
								This will start the archive process which will (depending on how much Facebook data you have) take a while to process.</p>
								<img src="/resources/img/fb-download-information.png" alt="Facebook download information" class="img-responsive img-thumbnail">
							</div>
						</div>

						<div class="row top-buffer">
   							<div class="col-xs-12">
								<h4>Step 4</h4>
								<p>You will be sent 2 emails to the email address associated with your Facebook account.<br/>
								<br/>
								The first email is just a notice to say you have recently requested a copy of your Facebook data. You can ignore this.<br/>
								<br/>
								You will receive the second email when Facebook as finished collating all your data and your archive is available to be downloaded. You should click on the download link in the email, which will take you back to Facebook where you can start downloading your Facebook archive.
								</p>
								<img src="/resources/img/fb-email-download.png" alt="Facebook download email" class="img-responsive img-thumbnail">
							</div>
						</div>

						<div class="row top-buffer">
							<div class="col-xs-12">
								<div class="alert alert-info" role="alert">
			   						<i class="fa fa-desktop" aria-hidden="true"></i> If using a mac, then these next steps will be very similar if not identical.
		   						</div>
		   					</div>
						</div>

						<div class="row">
   							<div class="col-xs-7">
	   							<img src="/resources/img/fb-html-folder.png" alt="Facebook html folder" class="img-responsive img-thumbnail">
	   							<br/><br/>
	   							<img src="/resources/img/fb-messages-file.png" alt="Facebook messages.htm file" class="img-responsive img-thumbnail">

   							</div>
   							<div class="col-xs-5">
   								<h4>Step 5</h4>
	   							<p>Once you have your Facebook archive, you want to double click on it which will open it up.<br/>
	   							<br/>
	   							Navigate to the folder called html and once there select the messages.htm file and drag it onto your desktop.<br/>
	   								<div class="alert alert-success" role="alert">
		   								<span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
		   								This is the file that you want to upload to this tool so it can extract your needed conversation history.
	   								</div>
	   							</p>
   							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-inverse" data-dismiss="modal">Close</button>
					</div>
				</div>
			</div>
		</div>

		<div class="modal fade" id="privacy-modal" tabindex="-1" role="dialog">
			<div class="modal-dialog modal-lg">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h3 class="modal-title">Please note that your privacy is very important to us</h3>
					</div>
					<div class="modal-body privacy-modal-body">
						<h4>Your files</h4>
						<p>By using this tool, you are uploading your personal message history from Facebook to allow this service to extract your conversations from it. Your files are not made available to anyone else or included in any kind of backups. As a policy, both the files uploaded by you and any copies produced while processing them are automatically deleted from our server after 20 minutes.</p>
						<h4>Data collected by us</h4>
						<p>We store information about your IP address, which browser you use, which actions you performed on our website. This data is collected to for internal analytical purposes in order to provide better services and to respond to any questions you may have after using our services. We do not store any of the content in your files.</p>
						<h4>Security</h4>
						<p>Our services are hosted on private servers with limited access. All services are available over an encrypted SSL (https) connection. Using an encrypted connection adds protection while files and other information is being sent back and forth over the Internet.</p>
						<h4>Cookies</h4>
						<p>We may use cookies on this site to provide services tailored to you and for insights into how our services are used. Third parties such as Google also use cookies on our sites for their own purposes. If cookies concern you, you can turn them off. You can still use these services.</p>

						<h3>Terms of Service</h3>
						<h4>No warranties</h4>
						<p>We strive to provide a service that works efficiently and without errors, and to provide support via email should you need it. However, any services provided on this website are provided "as is" without warranty of any kind. We do not guarantee their availability, accuracy, completeness, or the availability of support related to them. We may add, remove, or change the services provided without notice.</p>
						<h4>Limitation of our liability</h4>
						<p>Your use of this website is at your own risk. Under no circumstances shall BSP Software Design Solutions, its suppliers, or any other party involved in creating, producing, or delivering this website's contents or services be liable to you or any other person for any indirect, direct, special, incidental, or consequential damages arising from your access to, or use of, this website.</p>
						<h4>Automated use</h4>
						<p>This website is meant to be used by human visitors. Any kind of automated use of this website without a written consent from BSP Software Design Solutions is prohibited. Such use is subject to being blocked without notice.</p>

						<p class="privacy-footer">The services on this website are offered by BSP Software Design Solutions, a company based in Perth, Australia.</br>
						</br>
						You are welcome to contact us via the Feedback button with any questions or concerns.</br>
						</br>
						<span class="pull-right">Last updated on 2016-07-15</span>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-inverse" data-dismiss="modal">Close</button>
					</div>
				</div>
			</div>
		</div>

		<div class="modal fade" id="feedback-modal" tabindex="-1" role="dialog">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title">Feedback / Contact Us</h4>
					</div>
					<div class="modal-body feedback-modal-body">
						{{ Form::open(array('id' => 'feedbackForm', 'class' => 'form-horizontal')) }}
						<div class="form-group">
							<label class="col-xs-3 control-label" for="feedbackName">Your name <span class="required"*</span></label>
							<div class="col-xs-8">
								{{ form::text('feedbackName', '', ['class' => 'form-control', 'required']) }}
							</div>
						</div>

						<div class="form-group">
							<label class="col-xs-3 control-label" for="email">Your email</label>
							<div class="col-xs-8">
								{{ form::email('emailAddress', '', ['class' => 'form-control', 'placeholder' => 'me@email.com', 'required']) }}
							</div>
						</div>

						<div class="form-group">
							<label class="col-xs-3 control-label" for="message">Message</label>
							<div class="col-xs-8">
								{{ form::textarea('message', '', ['class' => 'form-control', 'placeholder' => 'message', 'rows' => 5, 'required']) }}
							</div>
						</div>

						<div class="form-group">
							<div class="col-xs-8 col-md-offset-3">
								<div class="g-recaptcha" data-sitekey="{{ env('RE_CAP_SITE') }}"></div>
							</div>
						</div>
						{{ Form::Close() }}
					</div>
					<div class="modal-footer">
						<button type="submit" class="btn btn-inverse" id="feedbackSumit">Submit</button>
					</div>
				</div>
			</div>
		</div>

    	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
		<script src="resources/js/bootstrap.min.js"></script>
		<script src="resources/js/jquery.validate.min.js"></script>
		<script src="resources/js/jquery.serialize-object.min.js"></script>
		<script src="resources/js/fileinput.min.js"></script>
		<script src="resources/js/bootstrap-select.min.js"></script>
		<script src="resources/js/js.cookie.min.js"></script>
		<script src="resources/js/main.js"></script>
	</body>
</html>
