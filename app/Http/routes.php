<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

// routes for our facebook controller
Route::post('facebook/upload', 'FacebookController@uploadHistory');
Route::post('facebook/extract', 'FacebookController@extractHistory');
Route::get('facebook/download/{hash}', 'FacebookController@downloadHistory');

// feedback/contact us
Route::post('feedback', 'FeedbackController@process');
