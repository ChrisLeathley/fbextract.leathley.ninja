<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

use Storage;
use App\Http\Requests;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Session;
use Symfony\Component\HttpFoundation\Response;

class FacebookController extends Controller
{
	private $excludeGroupConverstations = true;

	/**
	* Upload the message.htm history file for furthor processing
	*
	* @param Request $request
	* @return string
	*/
    public function uploadHistory(Request $request)
    {
    	$response = [
    		'status' => 'ok',
    		'message' => '',
    		'metadata' => array()
    	];

    	// make sure we have a uploaded file
    	if ($request->hasFile('historyFile')) {

    		// load the file from temporary storage
			$fullMessageHistory	= file_get_contents($request->file('historyFile')->getRealPath());

    		// make a copy of the file so we can reference if going forward (as the uploaded file is deleted at the end of the request)
			$fileName = bin2hex(random_bytes(24));
			$fullFileName = 'uploads/' . $fileName;
			Storage::put($fullFileName,	$fullMessageHistory);

			// Get the owner of the facebook account (between the first h1 tag)
			$result = preg_match_all('/<h1>(.*?)<\/h1>/s', $fullMessageHistory, $iAmMatches);
			$iAm = trim($iAmMatches[1][0]);

			// Load the history into a dom element
			$dom = new \DOMDocument();
			$dom->loadHTML('<?xml encoding="UTF-8">' . $fullMessageHistory);
			$dom->preserveWhiteSpace = false;

			$xpath = new \DOMXPath($dom);

			// not needed any more, free some memory as this file can be quiet large
			$fullMessageHistory = '';

			$peopleIveChattedWith = array();

			// go through and find all the threads (conversations between two (or more) people) and get who they are between
			$threadDomList = $xpath->query("//div[@class='thread']");
			foreach ($threadDomList as $index => $threadDom) {

				// build up a unique list of people 'iAm' has chatted with
				$peopleInConversation = $threadDom->firstChild->nodeValue;

				// 'iAm' must be in the conversation (and not granted access to a conversation group. that is not the purpose of this tool)
				if (false !== stripos($peopleInConversation, $iAm)) {
					$people = explode(', ', $peopleInConversation);

					if ((true == $this->excludeGroupConverstations) && (count($people) != 2)) {
						continue;
					}

					foreach ($people as $person) {
						// add them to the people 'iAm' chatted with (use the persons name as the primary key so they are only added once)
						if (trim($person) != $iAm) {
							$peopleIveChattedWith[strtolower(trim($person))] = trim($person);
						}
					}
				}
			}

			// sort the list into alphabetical order (use a collator to sort in a natural order with non english characters) (requires php5_intl)
			$collator = new \Collator('en_AU');
			$collator->sort($peopleIveChattedWith);

			$response['metadata'] = [
				'file' => $fileName,
				'iAm' => $iAm,
				'peopleIveChattedWith' => array_values($peopleIveChattedWith)
			];
		}
		else {
	    	$response = [
    			'status' => 'error',
    			'message' => 'No File Uploaded'
			];
		}

    	return Response()->json($response);
    }

    /**
    * Extract the Message Conversation history from the uploaded message.htm file
    *
    * @param Request $request
    * @return download
    */
    public function extractHistory(Request $request)
    {
    	$response = [
    		'status' => 'ok',
    		'message' => '',
    		'metadata' => array()
		];

		// this REST controller can take quiet a file to process. Allow upto 90 seconds to process
		ini_set('max_execution_time', 90);

		// make sure we get our metadata
		if ($request->has('metadata')) {

			$metadata = json_decode($request->get('metadata'), true);

			/*	metadata.fileToProcess
				metadata.iAmCombo
				metadata.whoWithCombo	*/

			$iAm = $metadata['iAmCombo'];
			$peopleIWantToExtract = explode(",", $metadata['whoWithCombo']);
			$fullFileName = 'uploads/' . $metadata['fileToProcess'];

			// make sure that the message history file is still available
			if (Storage::has($fullFileName)) {
				// load it in
				$fullMessageHistory = Storage::get($fullFileName);

				// and into a dom element
				$dom = new \DOMDocument();
				$dom->loadHTML('<?xml encoding="UTF-8">' . $fullMessageHistory);
				$dom->preserveWhiteSpace = false;

				$xpath = new \DOMXPath($dom);

				// not needed any more, free some memory as this file can be quiet large
				$fullMessageHistory = '';

				// work out the longest name (so we can nicely pad out the output)
				$longestNameLength = strlen($iAm);
				foreach ($peopleIWantToExtract as $person) {
					$longestNameLength = max($longestNameLength, strlen($person));
				}

				/*
				 * Split the message history into threads (conversations between (or more) two people)
				 */
				$threadsToProcess = array();
				$threadDomList = $xpath->query("//div[@class='thread']");
				foreach ($threadDomList as $index => $threadDom) {

					$peopleInConversation = $threadDom->firstChild->nodeValue;

					// 'iAm' must be in the conversation (and not granted access to a conversation group. that is not the purpose of this tool)
					if (false !== stripos($peopleInConversation, $iAm)) {

						// are wanting to include this thread (must contain people we are looking for) ?
					    $people = explode(', ', $threadDom->firstChild->nodeValue);

    					// if excluding conversations then there must only be two people
						if ((true == $this->excludeGroupConverstations) && (count($people) != 2)) {
							continue;
						}

						// count how many people we are looking for are
					    $foundCount = 0;
					    foreach ($peopleIWantToExtract as $person) {
    						if (false !== in_array($person, $people)) {
    							$foundCount++;
    						}
					    }

					    if ($foundCount) {
						    // becuase facebook limits the number of message per thread to 10,000 we have to get all the thread dates and sort them
						    // (they may not be in order in the original data)
						    // facebook also don't give us a second time value which would simplify this

							// get the meta span (contains the date)
	    					$metaData = $xpath->query("div[@class='message']/div[@class='message_header']/span[@class='meta']", $threadDom);

							$unixTime = $this->_convertFacebookTimestampToUnix($metaData->item(0)->nodeValue);
							$isoTime = date("c", $unixTime);
							$threadsToProcess[$isoTime] = $threadDom;
					    }
					}
				}

				// sort the treads in date order (based on the date of the first message in the thread)
				ksort($threadsToProcess);

				/*
				 *
				 */
				$totalMessageHistoryArray = array();
				foreach ($threadsToProcess as $threadDom) {

					$messageHistoryArray = array();

					// loop over the thread in pairs
					$threadMessagesDom = $threadDom->childNodes;
					for ($i=1; $i<$threadMessagesDom->length; $i+=2) {
						/*
							Messages are split into two parts. A div with the header information and a separate p tag with the actual message
							(why can't it be in the message div :/)

							<div class="message">
								<div class="message_header">
									<span class="user">Chris Leathley</span>
									<span class="meta">Saturday, 15 August 2015 at 19:21 UTC+08</span>
								</div>
							</div>
							<p>ok. talk to you later.</p>
						*/

						$headerIndex  = $i;
						$metaData = $xpath->query("div[@class='message_header']/span[@class='meta']", $threadMessagesDom->item($headerIndex));
						$userData = $xpath->query("div[@class='message_header']/span[@class='user']", $threadMessagesDom->item($headerIndex));

						$messageIndex = $i + 1;
						$messageHistoryArray[] = array(
							'who' => str_pad($userData->item(0)->nodeValue, $longestNameLength),
							'when' => date('D, dS M Y H:i', $this->_convertFacebookTimestampToUnix($metaData->item(0)->nodeValue)),
							'msg' => trim($threadMessagesDom->item($messageIndex)->nodeValue)
						);
					}

					$messageHistoryArray = array_reverse($messageHistoryArray);
					$totalMessageHistoryArray[] = $messageHistoryArray;
				}

				/*
				 * Now build the final output and return it as a download to the user
				 */

				$lineEnding = "\r\n";

				// make a temporary file (in the processed folder)
				$fileName = bin2hex(random_bytes(24));
				$fileNameWithPath = '/processed/' . $fileName . '.txt';

				// ensure that the processed directory exists
				Storage::makeDirectory('processed');

				// save the name of the file in a separte file so we can set it when the use downloads it
				Storage::put($fileNameWithPath . '.name', $iAm . ' - ' . implode(', ', $peopleIWantToExtract) . ' - History.txt');

				// don't use the Storage functions as they are quiet slow for this sort of thing
				$fullFileName = storage_path('app' . $fileNameWithPath);
				$fp = fopen($fullFileName, 'w');
				foreach ($totalMessageHistoryArray as $messageHistoryArray) {
					foreach ($messageHistoryArray as $record) {
						$line = $record['who'] . ' - ' . $record['when'] . ' - ' . $record['msg'] . $lineEnding;
						fwrite($fp, $line);
					}
				}
				fclose($fp);

				// return the processed history reference
				$response['metadata']['hash'] = $fileName;
			}
			else {
				$response['status'] = 'error';
				$response['message'] = 'Unable to find your Message History (may of been purged due to age).';
			}
		}
		else {
	    	$this->_convertFacebookTimestampToUnix(time());
		}

    	return Response()->json($response);
	}

	/**
	* Download a processed history extraction
	*
	* @param Request $request
	* @param string $hash
	*/
	public function downloadHistory(Request $request, $hash) {
		$fullFileName = 'processed/' . $hash . '.txt';
		if (Storage::exists($fullFileName)) {

			$downloadName = 'History.txt';
			if (Storage::exists($fullFileName . '.name')) {
				$downloadName = Storage::get($fullFileName . '.name');
			}

			// return the history file as an inline attachment
			return response()->download(storage_path('app') . '/' . $fullFileName, $downloadName, [ 'content-type: plain/text' ]);
		}
		else {
			// return a 404
			return Response()->make('Not Found', 404);
		}
	}

	/**
	* convert the facebook timestamp into a unix time_t which we can then format how we like
	*
	* @param string $facebookTimestamp
	* @return int
	*/
	private function _convertFacebookTimestampToUnix($facebookTimestamp) {
		// Saturday, 15 August 2015 at 19:39 UTC+08

		// 'at' is non standard, convert it to a normal separator
		$facebookTimestamp = str_replace(' at ', ', ', $facebookTimestamp);
		// kill off the UTC is it confuses strtotime
		$facebookTimestamp = str_replace('UTC', '', $facebookTimestamp);

		return strtotime($facebookTimestamp);
	}
}

?>
