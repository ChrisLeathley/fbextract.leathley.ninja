<?php

namespace App\Http\Controllers;

use Mail;
use Illuminate\Http\Request;
use App\Http\Requests;
use ReCaptcha\ReCaptcha;

class FeedbackController extends Controller
{
    /**
    * Extract the Message Conversation history from the uploaded message.htm file
    *
    * @param Request $request
    * @return download
    */
    public function process(Request $request)
    {
    	$response = [
    		'status' => 'ok',
    		'message' => 'Thank you. Your feedback has been submitted.'
		];

		$secret =  env('RE_CAP_SECRET');
		$recaptchaResponse = $request->get('g-recaptcha-response');
        $remoteIp = $_SERVER['REMOTE_ADDR'];

		$recaptcha = new ReCaptcha($secret);
        $recaptchaVerifyResponse = $recaptcha->verify($recaptchaResponse, $remoteIp);
        if ($recaptchaVerifyResponse->isSuccess()) {
			$metadata = json_decode($request->get('metadata'), true);

			$subject = 'Facebook Extractor Feedback';
			$fromName = $metadata['feedbackName'];
			$fromEmail = $metadata['emailAddress'];
			$data = $metadata['message'];

			$toEmail = 'chris@leathley.ninja';
			$toName = 'Facebook Extract Feedback';

			Mail::send('emails.contactus', ['data' => $data], function ($message) use ($fromEmail, $fromName, $subject, $toEmail ) {
				$message->subject($subject);
			    $message->from($fromEmail, $fromName);
			    $message->to($toEmail);	//)->cc($fromEmail);
			});
		}
		else {
			$response['status'] = 'error';
			$response['message'] = 'Unable to verify reCAPTCHA';
		}

    	return Response()->json($response);
	}
}
