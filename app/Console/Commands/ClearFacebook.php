<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use Storage;
use \App\Http\Controllers\FacebookController;

class ClearFacebook extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:clear-facebook';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Clear temporary files created by the facebook exporter';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $expiryPeriod = strtotime('-20 minutes');

        $allFiles = Storage::files('uploads');
        foreach ($allFiles as $file) {
			$lastModified = Storage::lastModified($file);
			if($lastModified <= $expiryPeriod) {
				Storage::delete($file);
			}
        }

        $allFiles = Storage::files('processed');
        foreach ($allFiles as $file) {
			$lastModified = Storage::lastModified($file);
			if($lastModified <= $expiryPeriod) {
				Storage::delete($file);
			}
        }
    }
}
