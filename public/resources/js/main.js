var $messageUploadWidget = null;

$(function() {

	// extend ajax to set the CSRF header (security requirement for laravel)
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	});

	// init the file uploader
	$messageUploadWidget = $("#historyFile").fileinput({
		browseLabel: 'Select Facebook messages.htm',
		showPreview: false,
		showRemove: false,
		showUpload: false,
		autoReplace: true,
		maxFileCount: 1,
		uploadAsync: true,
		maxFileSize: 20000,		//in KB
		uploadUrl: "facebook/upload",
		allowedFileExtensions: ['htm'],
		browseClass: 'btn btn-inverse',
		progressClass: 'progress-bar progress-bar-danger progress-bar-striped',
		progressCompleteClass: 'progress-bar progress-bar-danger',
		fileTypeSettings: {
		    text: function(vType, vName) {
		        return typeof vType !== "undefined" && vType.match('text.*') || vName.match(/messages\.htm$/i);
		    }
		}
	}).on('fileuploaded', function(event, data, previewId, index) {
        var form = data.form, files = data.files, extra = data.extra, response = data.response, reader = data.reader;

        // get a reference to our metadata
		var metadata = response.metadata;

		// set the hash of the file
		$('#fileToProcess').val(metadata.file);
		// set who i am
		$('#iAmCombo').find('option').remove();
		$('#iAmCombo').append(new Option(metadata.iAm, metadata.iAm));
		//$('#iAmCombo').prop('title', 'Nobody selected');
		$('#iAmCombo').selectpicker('val', metadata.iAm);

		// populate the who with combo
		$('#whoWithCombo').find('option').remove();
		$.each(metadata.peopleIveChattedWith, function(index, value) {
			$('#whoWithCombo').append(new Option(value, value));
		});
		//$('#whoWithCombo').prop('title', 'Please select one of the following people');
		$('#whoWithCombo').selectpicker('val', metadata.peopleIveChattedWith.shift());

		// enable step 2 (pick people to process)
		disableStep2(false);
	});

	$("#historyFileSubmit").bind('click', function(event) {
		$("#historyFileUploadForm").removeClass('has-error');

		// if the upload is valid, process it (this will call the 'fileuploaded' event when done)
		if (0 == $messageUploadWidget.val().length) {
			$("#historyFileUploadForm").addClass('has-error');
		}
		else {
			$messageUploadWidget.fileinput("upload");
		}

		// track action on GA
		ga('send', {
			hitType: 'event',
			eventCategory: 'History',
			eventAction: 'Upload'
		});

		// don't actually submit the form
		event.preventDefault();
	});

	$("#conversationExtractSubmit").bind('click', function(event) {

		$('#iAmCombo').closest('.form-group').removeClass('has-error');
		$('#whoWithCombo').closest('.form-group').removeClass('has-error');

		var iAm = $('#iAmCombo').selectpicker('val');
		var whoWith = $('#whoWithCombo').selectpicker('val');
		if ((iAm !== null) && (whoWith !== null)) {
			// serialise the form
			var json = $("form#conversationSelectForm").serializeJSON();

			// show the processing modal
			//var gifSource = $('#processingGif').attr('src'); //get the source in the var
    		//$('#processingGif').attr('src', ""); //erase the source
    		//$('#processingGif').attr('src', gifSource+"?"+new Date().getTime());
			//document.getElementById('processing-modal').innerHTML = document.getElementById('processing-modal').innerHTML;

			var pb = document.getElementById("processingGif");
			pb.style.display = '';
			pb.innerHTML = '<img src="/resources/img/processing.gif?' + new Date().getTime() + '" width="128" height="128" />';

			$('#processing-modal').modal('show');

			// track action on GA
			ga('send', {
				hitType: 'event',
				eventCategory: 'History',
				eventAction: 'Extract'
			});

			// post to the extract controller
			$.ajax({
				type: "POST",
				url: 'facebook/extract',
				dataType: 'json',
				data: {
					metadata: json
				},
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				success: function(data, status) {
					$('#processing-modal').modal('hide');

					window.location = 'facebook/download/' + data.metadata.hash;
				},
				error: function (jqXHR, status, error) {
					$('#processing-modal').modal('hide');
					alert(error);
				}
			});
		}
		else {
			if (iAm === null) {
				$('#iAmCombo').closest('.form-group').addClass('has-error');
			}

			if (whoWith === null) {
				$('#whoWithCombo').closest('.form-group').addClass('has-error');
			}
		}
	});

	// style up the combo box's
	$('#iAmCombo, #whoWithCombo').selectpicker({
		'size' : 10,
		'noneSelectedText' : 'Nobody Selected'
	});

	// bind the help modals
	$('.js-how-to-use').bind('click', function(event) {
		// track action on GA
		ga('send', {
			hitType: 'event',
			eventCategory: 'View',
			eventAction: 'How-To-Use'
		});

		$('#how-to-use-modal').modal('show');
		event.preventDefault();
	});

	$('.js-privacy').bind('click', function(event) {
		// track action on GA
		ga('send', {
			hitType: 'event',
			eventCategory: 'View',
			eventAction: 'Privacy'
		});

		$('#privacy-modal').modal('show');
		event.preventDefault();
	});

	$('.js-feedback').bind('click', function(event) {
		// track action on GA
		ga('send', {
			hitType: 'event',
			eventCategory: 'View',
			eventAction: 'Feedback'
		});

		// reset the form (and validation errors)
		$('form#feedbackForm').trigger('reset');
        $("form#feedbackForm").validate().resetForm();
        $("form#feedbackForm").find('.form-group').removeClass('has-error');
		grecaptcha.reset();

		$('#feedback-modal').modal('show');
		event.preventDefault();
	});

	$("#feedbackSumit").bind('click', function(event) {
		$('form#feedbackForm').validate();
		if ($("form#feedbackForm").valid()) {
			var json = $("form#feedbackForm").serializeJSON();

			// post to the extract controller
			$.ajax({
				type: "POST",
				url: 'feedback',
				dataType: 'json',
				data: {
					metadata: json,
					'g-recaptcha-response': grecaptcha.getResponse()
				},
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				success: function(data, status) {
					if ('error' == data.status) {
						alert(data.message);
					}
					else {
						$('#feedback-modal').modal('hide');
					}
				},
				error: function (jqXHR, status, error) {
					$('#feedback-modal').modal('hide');
					alert(error);
				}
			});
		}
		event.preventDefault();
	});

	// override jquery validate plugin error defaults to work nicely with bootstrap
	$.validator.setDefaults({
		highlight: function(element) {
			$(element).closest('.form-group').addClass('has-error');
		},
		unhighlight: function(element) {
			$(element).closest('.form-group').removeClass('has-error');
		},
		errorElement: 'span',
		errorClass: 'help-block',
		errorPlacement: function(error, element) {
			if(element.parent('.input-group').length) {
				error.insertAfter(element.parent());
			} else {
				error.insertAfter(element);
			}
		}
	});

	// disable step 2 until the history file is updated.
	disableStep2(true);
});

function disableStep2(state) {
	// set/clear the disabled property
	$('#iAmCombo, #whoWithCombo, #conversationExtractSubmit').prop('disabled', state);
	// add/remove the bootstrap disabled class
	if (state) {
		$('#iAmCombo, #whoWithCombo, #conversationExtractSubmit').addClass('disabled');
	}
	else {
		$('#iAmCombo, #whoWithCombo, #conversationExtractSubmit').removeClass('disabled');
	}
	// refesh any components
	$('#iAmCombo, #whoWithCombo').selectpicker('refresh');
}
